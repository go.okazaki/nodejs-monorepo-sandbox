TARGETS = \
	packages \
	services/api \
	services/web

.PHONY: $(TARGETS)

all: $(TARGETS)

upgrade: $(TARGETS)

build: $(TARGETS)

test: $(TARGETS)

install: $(TARGETS)

clean: $(TARGETS)

packages:
	cd $@ && $(MAKE) $(MAKECMDGOALS)

services/api: packages
	cd $@ && $(MAKE) $(MAKECMDGOALS)

services/web: packages
	cd $@ && $(MAKE) $(MAKECMDGOALS)

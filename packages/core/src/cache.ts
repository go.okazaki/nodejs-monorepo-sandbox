import LRU from 'lru-cache' // https://github.com/isaacs/node-lru-cache

interface Options<T, V> {
  valueFactory: ValueFactory<T, V>
  store?: ValueStore<V>
  keyConverter?: KeyConverter<T>
}
type Key = object | string | number | boolean
type KeyConverter<T> = (target: T) => Key | Promise<Key>
type ValueFactory<T, V> = (target: T) => V | Promise<V>
interface ValueStore<V> {
  get(key: Key): V | undefined
  set(key: Key, value: V): this
  delete(key: Key): boolean
}

export class ValueCache<T, V> {
  readonly valueFactory: ValueFactory<T, V>
  readonly keyConverter: KeyConverter<T>
  readonly store: ValueStore<V>

  constructor(options: Options<T, V>) {
    this.valueFactory = options.valueFactory
    this.keyConverter = options.keyConverter ?? ((t) => t as Key)
    this.store = options.store ?? new Map<Key, V>()
  }

  static weak<T, V>(
    valueFactory: ValueFactory<T, V>,
    keyConverter?: KeyConverter<T>,
  ): ValueCache<T, V> {
    return new ValueCache<T, V>({
      valueFactory,
      keyConverter,
      store: new WeakMap<object, V>(),
    })
  }

  static lru<T, V>(
    valueFactory: ValueFactory<T, V>,
    options: LRU.Options<Key, V>,
    keyConverter?: KeyConverter<T>,
  ): ValueCache<T, V> {
    return new ValueCache<T, V>({
      valueFactory,
      keyConverter,
      store: new LRU<Key, V>(options),
    })
  }

  async get(target: T): Promise<V> {
    const key = await Promise.resolve(this.keyConverter(target))
    let value = this.store.get(key)
    if (!value) {
      value = await Promise.resolve(this.valueFactory(target))
      this.store.set(key, value)
    }
    return value
  }

  async delete(target: T): Promise<boolean> {
    const key = await Promise.resolve(this.keyConverter(target))
    return this.store.delete(key)
  }
}

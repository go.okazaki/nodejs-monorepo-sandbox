import { randomString } from '..'

describe('util', () => {
  it('randomString', async () => {
    const result = randomString(10)
    expect(result.length).toEqual(10)
    expect(result).not.toEqual(randomString(10))
  })
})

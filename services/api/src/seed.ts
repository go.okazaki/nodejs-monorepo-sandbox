import assert from 'assert'
import { PrismaClient } from '@prisma/client'
import { Claim, ClaimType, generateToken } from './token'
import { redisClient } from './redis'

const prisma = new PrismaClient()

async function main() {
  const email = process.env.ADMIN_EMAIL
  assert(email)
  const admin = {
    name: 'Administrator',
    email,
    enabled: true,
    type: ClaimType.ADMIN,
  }
  const result = await prisma.user.upsert({
    where: { email: admin.email },
    create: admin,
    update: admin,
  })
  const token = await generateToken(new Claim(result.id, result.type))
  console.log(`Administrator token:${token}`)
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
    await redisClient.close()
  })

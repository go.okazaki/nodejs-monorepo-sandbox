// https://github.com/devoxa/prisma-relay-cursor-connection
import {
  Connection,
  ConnectionArguments,
  Edge,
  findManyCursorConnection,
  Options,
  PrismaFindManyArguments,
} from '@devoxa/prisma-relay-cursor-connection'
import { decodeBase64, encodeBase64 } from '@next-graphql-sandbox/core'
import { Decimal } from '@prisma/client/runtime'
import { GraphQLResolveInfo } from 'graphql'
import {
  FilterDateTimeInput,
  FilterFloatInput,
  FilterIntInput,
  FilterStringInput,
  FilterTextInput,
  InputMaybe,
} from './generated/graphql'

export type CursorConnection<T> = Connection<T, Edge<T>>

export const emptyConnection = {
  edges: [],
  totalCount: 0,
  pageInfo: {
    hasNextPage: false,
    hasPreviousPage: false,
  },
}

export async function toCursorConnection<
  R = {
    id: string
  },
  N = R,
  C = {
    id: string
  },
>(
  findMany: (args: PrismaFindManyArguments<C>) => Promise<R[]>,
  aggregate: () => Promise<number>,
  args?: ConnectionArguments,
  recordToNode = (record: R): N => record as unknown as N,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getCursor = (record: R): C => ({ id: (record as any).id } as unknown as C),
  encodeCursor = (cursor: C): string => encodeBase64<C>(cursor),
  decodeCursor = (cursorString: string): C => decodeBase64<C>(cursorString),
  resolveInfo: GraphQLResolveInfo | null = null,
): Promise<CursorConnection<N>> {
  const newOptions: Options<R, C, N, Edge<N>> = {
    getCursor,
    encodeCursor,
    decodeCursor,
    recordToEdge: (record) => ({ node: recordToNode(record) }),
    resolveInfo,
  }
  return await findManyCursorConnection(findMany, aggregate, args, newOptions)
}

type StringFilter = {
  equals?: string
  in?: Array<string>
  notIn?: Array<string>
  contains?: string
  startsWith?: string
  endsWith?: string
  search?: string
  not?: StringFilter
}

type StringNullableFilter = {
  equals?: string | null
  in?: Array<string>
  notIn?: Array<string>
  contains?: string
  startsWith?: string
  endsWith?: string
  search?: string
  not?: StringNullableFilter
}

export function toStringFilter(
  filter?: InputMaybe<FilterStringInput>,
): StringFilter | undefined {
  return filter
    ? {
        equals: filter.equals ?? undefined,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        contains: filter.contains ?? undefined,
        startsWith: filter.startsWith ?? undefined,
        endsWith: filter.endsWith ?? undefined,
        not: toStringFilter(filter.not),
      }
    : undefined
}

export function toStringNullableFilter(
  filter?: InputMaybe<FilterStringInput>,
): StringNullableFilter | undefined {
  return filter
    ? {
        equals: filter.equals,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        contains: filter.contains ?? undefined,
        startsWith: filter.startsWith ?? undefined,
        endsWith: filter.endsWith ?? undefined,
        not: toStringNullableFilter(filter.not),
      }
    : undefined
}

export function toTextNullableFilter(
  filter?: InputMaybe<FilterTextInput>,
): StringNullableFilter | undefined {
  return filter
    ? {
        contains: filter.contains ?? undefined,
        startsWith: filter.startsWith ?? undefined,
        endsWith: filter.endsWith ?? undefined,
        search: filter.search ?? undefined,
      }
    : undefined
}

type IntFilter = {
  equals?: number
  in?: Array<number>
  notIn?: Array<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: IntFilter
}

type IntNullableFilter = {
  equals?: number | null
  in?: Array<number>
  notIn?: Array<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: IntNullableFilter
}

export function toIntFilter(
  filter?: InputMaybe<FilterIntInput>,
): IntFilter | undefined {
  return filter
    ? {
        equals: filter.equals ?? undefined,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toIntFilter(filter.not),
      }
    : undefined
}

export function toIntNullableFilter(
  filter?: InputMaybe<FilterIntInput>,
): IntNullableFilter | undefined {
  return filter
    ? {
        equals: filter.equals,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toIntNullableFilter(filter.not),
      }
    : undefined
}

type DecimalFilter = {
  equals?: Decimal | number | string
  in?: Array<Decimal> | Array<number> | Array<string>
  notIn?: Array<Decimal> | Array<number> | Array<string>
  lt?: Decimal | number | string
  lte?: Decimal | number | string
  gt?: Decimal | number | string
  gte?: Decimal | number | string
  not?: DecimalFilter
}

type DecimalNullableFilter = {
  equals?: Decimal | number | string | null
  in?: Array<Decimal> | Array<number> | Array<string>
  notIn?: Array<Decimal> | Array<number> | Array<string>
  lt?: Decimal | number | string
  lte?: Decimal | number | string
  gt?: Decimal | number | string
  gte?: Decimal | number | string
  not?: DecimalNullableFilter
}

export function toDecimalFilter(
  filter?: InputMaybe<FilterFloatInput>,
): DecimalFilter | undefined {
  return filter
    ? {
        equals: filter.equals ?? undefined,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toDecimalFilter(filter.not),
      }
    : undefined
}

export function toDecimalNullableFilter(
  filter?: InputMaybe<FilterFloatInput>,
): DecimalNullableFilter | undefined {
  return filter
    ? {
        equals: filter.equals,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toDecimalNullableFilter(filter.not),
      }
    : undefined
}

type DateTimeFilter = {
  equals?: Date
  in?: Array<Date> | Array<string>
  notIn?: Array<Date> | Array<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: DateTimeFilter
}

type DateTimeNullableFilter = {
  equals?: Date | null
  in?: Array<Date> | Array<string>
  notIn?: Array<Date> | Array<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: DateTimeNullableFilter
}

export function toDateTimeFilter(
  filter?: InputMaybe<FilterDateTimeInput>,
): DateTimeFilter | undefined {
  return filter
    ? {
        equals: filter.equals ?? undefined,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toDateTimeFilter(filter.not),
      }
    : undefined
}

export function toDateTimeNullableFilter(
  filter?: InputMaybe<FilterDateTimeInput>,
): DateTimeNullableFilter | undefined {
  return filter
    ? {
        equals: filter.equals,
        in: filter.in ?? undefined,
        notIn: filter.notIn ?? undefined,
        gt: filter.gt ?? undefined,
        lt: filter.lt ?? undefined,
        gte: filter.gte ?? undefined,
        lte: filter.lte ?? undefined,
        not: toDateTimeNullableFilter(filter.not),
      }
    : undefined
}

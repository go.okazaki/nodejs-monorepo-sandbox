import { Chance } from 'chance'
import { elasticsearchClient } from '../elasticsearch'

const chance = new Chance()

describe('elasticsearch', () => {
  const alias = 'sandbox-test'
  it('crud', async () => {
    await elasticsearchClient.createIndex(alias)

    const name = chance.name()
    const id = await elasticsearchClient.saveDocument(alias, {
      name,
      createdAt: new Date(),
    })
    let document = await elasticsearchClient.getDocument(alias, id)
    expect(document?.name).toBe(name)

    const newName = chance.name()
    await elasticsearchClient.saveDocument(alias, {
      id,
      name: newName,
      createdAt: new Date(),
    })

    document = await elasticsearchClient.getDocument(alias, id)
    expect(document?.name).toBe(newName)

    const deleted = await elasticsearchClient.deleteDocument(alias, id)
    expect(deleted).toBe(true)
  })

  it('scenario reIndex', async () => {
    const deleteIndexResult = await elasticsearchClient.deleteIndex(alias)
    expect(deleteIndexResult).toBeDefined()

    let names = await elasticsearchClient.getIndexNames(alias)
    expect(names.primary).toBe(`${alias}-0`)
    expect(names.backup).toBe(`${alias}-1`)

    const createIndexResult = await elasticsearchClient.createIndex(alias)
    expect(createIndexResult).toBe(true)

    names = await elasticsearchClient.getIndexNames(alias)
    expect(names.primary).toBe(`${alias}-0`)
    expect(names.backup).toBe(`${alias}-1`)

    const id = await elasticsearchClient.saveDocument(alias, {
      name: chance.name(),
      createdAt: new Date(),
    })
    expect(id).toBeDefined()

    const reIndexResult = await elasticsearchClient.reIndex(alias, 2)
    expect(reIndexResult).toBeDefined()

    names = await elasticsearchClient.getIndexNames(alias)
    expect(names.primary).toBe(`${alias}-1`)
    expect(names.backup).toBe(`${alias}-0`)

    const document = await elasticsearchClient.getDocument(alias, id)
    expect(document).toBeDefined()
  })

  it('searchDocuments pagination', async () => {
    await elasticsearchClient.createIndex(alias)

    await elasticsearchClient.saveDocument(alias, {
      name: chance.name(),
      createdAt: new Date(),
    })
    let result = await elasticsearchClient.searchDocuments({
      index: alias,
      sort: 'createdAt:desc',
      connection: {
        first: 2,
      },
    })
    const firstIds = result.edges.map((e) => e.node.id)
    const after = result.pageInfo.endCursor
    result = await elasticsearchClient.searchDocuments({
      index: alias,
      sort: 'createdAt:desc',
      connection: {
        first: 2,
        after,
      },
    })
    const secondIds = result.edges.map((e) => e.node.id)
    expect(firstIds.some((e) => secondIds.includes(e))).toBe(false)
  })

  it('searchDocuments template', async () => {
    await elasticsearchClient.deleteIndex(alias)
    await elasticsearchClient.createIndex(alias)
    // await elasticsearchClient.reIndex(alias)

    await elasticsearchClient.saveDocument(alias, {
      properties: {
        name: {
          string_ja:
            'AWSが主導するElasticsearchのフォーク「OpenSearch」にCanonicalが参加へ',
          string_en:
            'Canonical to join OpenSearch, a fork of Elasticsearch led by AWS.',
        },
        detail: {
          text_ja: `Linuxの主要なディストリビューションの1つであるUbuntuを開発するCanonicalは、AWSが主導するオープンソースの検索エンジン「OpenSearch」のプロジェクトへの参加を表明しました。
          AWSはオープンソースで開発されていたElasticsearchをマネージドサービスとして提供する「Amazon Elasticsearch Service」を提供していましたが、Elasticsearchの開発元であるElastic CEOのShay Banon氏は、これについてオープンソースの商標を軽視するなどAWSを強く非難する声明を2021年1月に発表しました。
          同時に、それまでApache License 2.0だったElasticsearchとKibanaのライセンスを、商用サービス化を制限する「Server Side Public License」（SSPL）と「Elastic License」のデュアルライセンスへ変更することも発表しました。
          すでにOpenSearchにはオラクルを始めとする多くのパートナーが参加していますが、オープンソースの主要なプレイヤーの1つであるCanonicalの参加はAWSやOpenSearchの継続性に懸念を持つ人々にとって心強いものとなりそうです。`,
          text_en: `Canonical, developer of Ubuntu, one of the major Linux distributions, has announced its participation in the AWS-led OpenSearch open source search engine project.
          AWS offered the Amazon Elasticsearch Service, a managed service for Elasticsearch, which had been developed as open source, but Elastic CEO Shay Banon, the developer of Elasticsearch In January 2021, Elastic CEO Shay Banon, the developer of Elasticsearch, issued a statement strongly criticizing AWS for disrespecting the open source trademark.
          At the same time, Elastic also announced that the licenses of Elasticsearch and Kibana, which were previously under the Apache License 2.0, would be changed to a dual license under the Server Side Public License (SSPL) and the Elastic License, which restricts commercialization as a service. The company also announced a change to a dual license of the Server Side Public License (SSPL) and the Elastic License, which restricts commercialization.
          While OpenSearch already has a number of partners participating, including Oracle, the addition of Canonical, one of the major open source players, will be reassuring to those concerned about the continuity of AWS and OpenSearch.`,
        },
      },
      createdAt: new Date(),
    })
    let result = await elasticsearchClient.searchDocuments({
      index: alias,
      q: 'オープンソース AWS 参加',
      sort: 'createdAt:desc',
    })
    expect(result.totalCount).toBeGreaterThan(0)

    result = await elasticsearchClient.searchDocuments({
      index: alias,
      q: 'open source AWS participation',
      sort: 'createdAt:desc',
    })
    expect(result.totalCount).toBeGreaterThan(0)
  })
})

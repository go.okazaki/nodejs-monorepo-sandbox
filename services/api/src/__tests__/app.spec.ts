import { getTestServer } from './setup'

describe('app', () => {
  it('apollo server health check', async () => {
    const server = await getTestServer()
    const result = await fetch(
      `http://localhost:${server.addressInfo.port}/.well-known/apollo/server-health`,
    )
    expect(result.status).toBe(200)
  })
})

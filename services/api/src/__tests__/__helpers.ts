import { Chance } from 'chance' // https://github.com/chancejs/chancejs
import { ConnectMutation } from '@next-graphql-sandbox/core/dist/generated/apiClient'
import { createApiService } from '@next-graphql-sandbox/core'
import { generatePrivilegeToken } from '../token'
import { getTestServer } from './setup'

export const chance = new Chance()

export async function getTestApiService() {
  const serverInfo = await getTestServer()
  const endpoint = `http://localhost:${serverInfo.addressInfo.port}/graphql`
  return createApiService(endpoint)
}

export async function testCreateUser(): Promise<
  ConnectMutation['connect']['user']
> {
  const apiService = await getTestApiService()
  const adminToken = await generatePrivilegeToken()
  apiService.setToken(adminToken)
  const { connect } = await apiService.connect({
    input: {
      email: chance.email(),
    },
  })
  return connect.user
}

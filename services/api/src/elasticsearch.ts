import { Client, ClientOptions } from '@elastic/elasticsearch'
import {
  MappingTypeMapping,
  Name,
  IndicesAlias,
  SearchResponseBody,
  IndicesIndexSettings,
} from '@elastic/elasticsearch/lib/api/types'
import { decodeBase64, encodeBase64 } from '@next-graphql-sandbox/core'

export interface DefaultTemplate {
  name: string
  index_patterns?: string | string[]
  mappings?: MappingTypeMapping
}

const defaultTemplateName = 'sandbox-default'
const defaultIndexPattern = 'sandbox-*'
const defaultIndexSettings: IndicesIndexSettings = {
  analysis: {
    analyzer: {
      text_ja_analyzer: {
        type: 'custom',
        char_filter: ['icu_normalizer', 'kuromoji_iteration_mark'],
        tokenizer: 'kuromoji_tokenizer',
        filter: [
          'kuromoji_baseform', // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-baseform.html
          'kuromoji_part_of_speech', // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-speech.html
          'ja_stop', // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-stop.html https://github.com/apache/lucene/blob/main/lucene/analysis/kuromoji/src/resources/org/apache/lucene/analysis/ja/stopwords.txt
          'kuromoji_number', // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-number.html
          'kuromoji_stemmer', // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-stemmer.html
        ],
      },
      string_ja_analyzer: {
        type: 'custom',
        char_filter: ['icu_normalizer'],
        tokenizer: 'kuromoji_tokenizer',
        filter: ['katakana_readingform', 'kuromoji_number', 'kuromoji_stemmer'],
      },
      text_en_analyzer: {
        type: 'custom',
        tokenizer: 'standard',
        filter: ['stop', 'stemmer'],
      },
      string_en_analyzer: {
        type: 'custom',
        tokenizer: 'standard',
        filter: ['stemmer'],
      },
    },
    filter: {
      // https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji-readingform.html
      romaji_readingform: {
        type: 'kuromoji_readingform',
        use_romaji: true,
      },
      katakana_readingform: {
        type: 'kuromoji_readingform',
        use_romaji: false,
      },
    },
  },
}
const defaultTypeMapping: MappingTypeMapping = {
  dynamic_templates: [
    {
      string_ja_template: {
        path_match: '*.string_ja',
        mapping: {
          type: 'text',
          analyzer: 'string_ja_analyzer',
        },
      },
    },
    {
      text_ja_template: {
        path_match: '*.text_ja',
        mapping: {
          type: 'text',
          analyzer: 'text_ja_analyzer',
        },
      },
    },
    {
      text_en_template: {
        path_match: '*.text_en',
        mapping: {
          type: 'text',
          analyzer: 'text_en_analyzer',
        },
      },
    },
    {
      string_en_template: {
        path_match: '*.string_en',
        mapping: {
          type: 'text',
          analyzer: 'string_en_analyzer',
        },
      },
    },
  ],
}

interface Document extends Record<string, unknown> {
  id: string
}

interface SaveDocument extends Record<string, unknown> {
  id?: string
}

type SearchParams = {
  index: string
  q?: string
  sort: string | string[]
  connection?: {
    first?: number
    after?: string
  }
}

type SearchResult<T> = {
  totalCount: number
  edges: Array<{ cursor: string; node: T }>
  pageInfo: {
    hasNextPage: boolean
    hasPreviousPage: boolean
    startCursor: string
    endCursor: string
  }
}

class ElasticsearchClient {
  private client: Client | undefined

  async getClient(): Promise<Client> {
    if (!this.client) {
      const options: ClientOptions = {
        node: process.env.ELASTICSEARCH_URL ?? 'http://localhost:9200',
      }
      const apiKey = process.env.ELASTICSEARCH_API_KEY
      if (apiKey) {
        options.auth = {
          apiKey,
        }
      }
      this.client = new Client(options)
    }
    return this.client
  }

  async init(): Promise<void> {
    const client = await this.getClient()
    await client.indices.putIndexTemplate({
      name: defaultTemplateName,
      index_patterns: defaultIndexPattern,
      template: {
        settings: defaultIndexSettings,
        mappings: defaultTypeMapping,
      },
    })
  }

  async createIndex(alias: string): Promise<boolean> {
    const client = await this.getClient()
    const index = `${alias}-0`
    const exists = await client.indices.exists({ index })
    if (exists) {
      return false
    }
    const aliases: Record<Name, IndicesAlias> = {}
    aliases[alias] = {}
    const response = await client.indices.create({
      index,
      aliases,
    })
    return response.acknowledged
  }

  async deleteIndex(alias: string): Promise<number> {
    const client = await this.getClient()
    const names = await this.getIndexNames(alias)
    let count = 0
    const primary = await client.indices.exists({
      index: names.primary,
    })
    if (primary) {
      await client.indices.delete({ index: names.primary })
      count++
    }
    const backup = await client.indices.exists({
      index: names.backup,
    })
    if (backup) {
      await client.indices.delete({ index: names.backup })
      count++
    }
    return count
  }

  async getIndexNames(
    alias: string,
  ): Promise<{ primary: string; backup: string }> {
    const client = await this.getClient()
    let b = false
    const exists = await client.indices.existsAlias({ name: alias })
    if (exists) {
      const response = await client.indices.getAlias({ name: alias })
      const index = Object.keys(response)[0]
      if (index) {
        const n = Number(index.substring(index.lastIndexOf('-') + 1))
        b = Boolean(n)
      }
    }
    return {
      primary: `${alias}-${b ? 1 : 0}`,
      backup: `${alias}-${b ? 0 : 1}`,
    }
  }

  async swap(alias: string): Promise<boolean> {
    const client = await this.getClient()
    const names = await this.getIndexNames(alias)
    const exists = await client.indices.existsAlias({ name: alias })
    if (exists) {
      await client.indices.deleteAlias({
        index: names.primary,
        name: alias,
      })
      await client.indices.putAlias({
        index: names.backup,
        name: alias,
      })
    } else {
      await client.indices.putAlias({
        index: names.primary,
        name: alias,
      })
    }
    return true
  }

  async reIndex(alias: string, shards?: number): Promise<boolean> {
    const client = await this.getClient()
    const names = await this.getIndexNames(alias)
    const primary = await client.indices.get({ index: names.primary })
    const state = primary[names.primary]
    const backup = await client.indices.exists({ index: names.backup })
    if (backup) {
      await client.indices.delete({ index: names.backup })
    }
    await client.indices.create({
      index: names.backup,
      settings: {
        number_of_shards: shards ?? state.settings?.number_of_shards,
      },
    })
    await client.reindex({
      wait_for_completion: true,
      refresh: true,
      source: {
        index: names.primary,
      },
      dest: {
        index: names.backup,
      },
    })
    return await this.swap(alias)
  }

  async getDocument<T extends Document>(
    index: string,
    id: string,
  ): Promise<T | undefined> {
    const client = await this.getClient()
    const response = await client.get<Document>({ id, index })
    return response.found
      ? ({ id: response._id, ...response._source } as T)
      : undefined
  }

  async saveDocument<T extends SaveDocument>(
    index: string,
    document: T,
  ): Promise<string> {
    const client = await this.getClient()
    const response = await client.index({
      id: document.id,
      index,
      refresh: true,
      document: {
        ...document,
        id: undefined,
      },
    })
    return response._id
  }

  async deleteDocument(index: string, id: string): Promise<boolean> {
    const client = await this.getClient()
    const response = await client.delete({
      id,
      index,
      refresh: true,
    })
    return response.result === 'deleted'
  }

  private toSearchResult<T extends Document>(
    response: SearchResponseBody<T>,
    first?: number,
    after?: string,
  ): SearchResult<T> {
    let totalCount: number
    const total = response.hits.total
    if (typeof total === 'object') {
      totalCount = total.value
    } else {
      totalCount = Number(total)
    }
    const hits = response.hits.hits
    const edges = hits.map((h) => {
      return {
        cursor: h.sort ? encodeBase64(h.sort) : '',
        node: {
          id: h._id,
          ...h._source,
        } as T,
      }
    })
    const startCursor = edges[0]?.cursor ?? undefined
    const endCursor = edges[edges.length - 1]?.cursor ?? undefined
    return {
      totalCount,
      edges,
      pageInfo: {
        hasNextPage: edges.length === first,
        hasPreviousPage: after ? true : false,
        startCursor,
        endCursor,
      },
    }
  }

  async searchDocuments<T extends Document>(
    params: SearchParams,
  ): Promise<SearchResult<T>> {
    const client = await this.getClient()
    const response = await client.search<T>({
      index: params.index,
      q: params.q,
      sort: params.sort,
      size: params.connection?.first ?? 10,
      search_after: params.connection?.after
        ? decodeBase64(params.connection?.after)
        : undefined,
    })
    return this.toSearchResult(
      response,
      params.connection?.first,
      params.connection?.after,
    )
  }
}

export const elasticsearchClient = new ElasticsearchClient()
elasticsearchClient.init().catch((e) => console.warn(e))

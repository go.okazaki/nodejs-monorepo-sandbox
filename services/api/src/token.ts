import assert from 'assert'
import crypto from 'crypto'
import jwt, { JwtPayload } from 'jsonwebtoken'
import { redisClient } from './redis'

export enum ClaimType {
  NORMAL = 1,
  ADMIN = 2,
}

export class Claim implements JwtPayload {
  readonly jti: string // override
  readonly type: number
  exp?: number
  constructor(readonly sub: string, type: ClaimType) {
    this.jti = crypto.randomUUID()
    this.type = type
  }

  static from(payload: JwtPayload): Claim {
    const sub = payload.sub
    assert(sub)
    const type = Number(payload['type'])
    const claim = new Claim(sub, type)
    Object.assign(claim, payload)
    return claim
  }

  setExpire(expireSec: number): void {
    this.exp = Math.floor(Date.now() / 1000 + expireSec)
  }

  getKey(): string {
    return `jwt.${this.sub}.${this.jti}`
  }

  toObject(): JwtPayload {
    return Object.assign({} as JwtPayload, this)
  }

  toJSON(): string {
    return JSON.stringify(this.toObject())
  }

  isAdmin(): boolean {
    return this.type === ClaimType.ADMIN
  }
}

type KeyPair = {
  privateKey: string
  publicKey: string
}

let keyPair: KeyPair | undefined
const JWT_SECRET_KEY = 'jwt.secret'

export async function generateKeyPair(): Promise<KeyPair> {
  const keyPair = await new Promise<KeyPair>((resolve, reject) =>
    crypto.generateKeyPair(
      'rsa',
      {
        modulusLength: 4096,
        publicKeyEncoding: { type: 'spki', format: 'pem' },
        privateKeyEncoding: { type: 'pkcs8', format: 'pem' },
      },
      (err, publicKey, privateKey) => {
        if (err) {
          reject(err)
        } else {
          resolve({ publicKey, privateKey })
        }
      },
    ),
  )
  return keyPair
}

async function getKeyPair(): Promise<KeyPair> {
  if (keyPair) {
    keyPair
  }
  const secret = await redisClient.get(JWT_SECRET_KEY)
  keyPair = secret ? JSON.parse(secret) : undefined
  if (!keyPair) {
    keyPair = await generateKeyPair()
    await redisClient.set(JWT_SECRET_KEY, JSON.stringify(keyPair))
  }
  return keyPair
}

async function getPrivateKey(): Promise<string> {
  const keyPair = await getKeyPair()
  return keyPair.privateKey
}

export async function getPublicKey(): Promise<string> {
  const keyPair = await getKeyPair()
  return keyPair.publicKey
}

export async function generateToken(
  input: Claim,
  expireSec = 0,
): Promise<string> {
  const privateKey = await getPrivateKey()
  let claim: Claim
  if (input instanceof Claim) {
    claim = input
  } else {
    claim = Claim.from(input)
  }
  if (0 < expireSec) {
    claim.setExpire(expireSec)
  }
  redisClient
    .set(claim.getKey(), claim.toJSON(), expireSec)
    .catch((e) => console.warn(e))
  const encoded = await new Promise<string | undefined>((resolve, reject) => {
    jwt.sign(
      claim.toObject(),
      privateKey,
      { algorithm: 'RS256' },
      (err, value) => {
        if (err) {
          reject(err)
        } else {
          resolve(value)
        }
      },
    )
  })
  assert(encoded)
  return encoded
}

export async function verifyToken(token: string): Promise<Claim | undefined> {
  const publicKey = await getPublicKey()
  let decoded
  try {
    decoded = await new Promise<string | jwt.JwtPayload | undefined>(
      (resolve, reject) => {
        jwt.verify(
          token,
          publicKey,
          { algorithms: ['RS256'] },
          (err, value) => {
            if (err) {
              reject(err)
            } else {
              resolve(value)
            }
          },
        )
      },
    )
  } catch (e) {
    console.warn(e)
  }
  let payload: JwtPayload | undefined
  if (typeof decoded === 'string') {
    payload = JSON.parse(decoded)
  } else if (decoded) {
    payload = decoded as JwtPayload
  }
  if (payload) {
    const claim = Claim.from(payload)
    const exists = await redisClient.exists(claim.getKey())
    return exists ? claim : undefined
  } else {
    return undefined
  }
}

export async function generatePrivilegeToken(origin?: Claim): Promise<string> {
  const claim = new Claim(origin?.sub ?? 'admin', ClaimType.ADMIN)
  return generateToken(claim)
}

import { AbilityBuilder, PureAbility } from '@casl/ability'
import { createPrismaAbility, PrismaQuery, Subjects } from '@casl/prisma' // https://github.com/stalniy/casl/tree/master/packages/casl-prisma
import { ValueCache } from '@next-graphql-sandbox/core'
import { User } from '@prisma/client'
import { Claim } from './token'

type Actions = 'create' | 'read' | 'update' | 'delete'
export type AppAbility = PureAbility<
  [
    Actions,
    Subjects<{
      User: User
    }>,
  ],
  PrismaQuery
>

const abilities = ValueCache.lru(
  buildAbility,
  { max: 100 },
  (claim) => claim?.jti ?? 'undefined',
)

async function buildAbility(claim: Claim | undefined): Promise<AppAbility> {
  const { can, cannot, build } = new AbilityBuilder<AppAbility>(
    createPrismaAbility,
  )
  if (claim) {
    can('read', 'User')
    if (claim.isAdmin()) {
      can('create', 'User')
      can('update', 'User')
      can('delete', 'User')
    } else {
      can('update', 'User')
      cannot('update', 'User', {
        NOT: { id: claim.sub },
      })
    }
  }
  return build()
}

export async function toAbility(claim: Claim | undefined): Promise<AppAbility> {
  return abilities.get(claim)
}

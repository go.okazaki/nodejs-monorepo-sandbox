import path from 'path'
import { loadSchemaSync } from '@graphql-tools/load'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader'
import { constraintDirective } from 'graphql-constraint-directive' // https://github.com/confuser/graphql-constraint-directive
import { GraphQLSchema } from 'graphql'
import { Resolvers } from './generated/graphql'
import {
  userMutationResolvers,
  userQueryResolvers,
  userResolvers,
} from './user'

const resolvers: Resolvers = {
  Query: {
    ...userQueryResolvers,
  },
  Mutation: {
    ...userMutationResolvers,
  },
  User: {
    ...userResolvers,
  },
}

const executableSchema = makeExecutableSchema({
  typeDefs: [
    loadSchemaSync(path.join(__dirname, 'graphql'), {
      loaders: [new GraphQLFileLoader()],
    }),
  ],
  resolvers,
})

const schema: GraphQLSchema = constraintDirective()(executableSchema)
export default schema

import assert from 'assert'
import cookie from 'cookie' // https://github.com/jshttp/cookie
import { Prisma, User as PUser } from '@prisma/client'
import { ForbiddenError } from 'apollo-server-express'
import { accessibleBy } from '@casl/prisma'
import { toCursorConnection } from '../prisma'
import {
  QueryResolvers,
  MutationResolvers,
  User,
  UserResolvers,
  UserConnection,
  ConnectionInput,
  InputMaybe,
  FilterUserInput,
  ConnectPayload,
} from '../generated/graphql'
import { Claim, ClaimType, generateToken } from '../token'
import { Context } from '../context'
import { redisClient } from '../redis'

export type Role = 'admin' | 'user'

export function toUser(source: PUser): User {
  return {
    ...source,
  }
}

export async function toUserWhereInput(
  context: Context,
  filter?: InputMaybe<FilterUserInput>,
): Promise<Prisma.UserWhereInput> {
  const ability = await context.getAbility()
  const where: Prisma.UserWhereInput = {
    ...accessibleBy(ability, 'read').User,
    enabled: filter?.enabled ?? true,
  }
  return where
}

export async function toUserConnection(
  context: Context,
  connection?: InputMaybe<ConnectionInput>,
  where?: Prisma.UserWhereInput,
): Promise<UserConnection> {
  const claim = await context.getClaim()
  assert(claim)
  const prisma = await context.getPrismaClient()
  const baseArgs: Prisma.UserFindManyArgs = {
    where,
    orderBy: [{ createdAt: 'desc' }],
  }
  const result = await toCursorConnection<PUser, User>(
    (args) =>
      prisma.user.findMany({
        ...args,
        ...baseArgs,
      }),
    () => prisma.user.count({ where }),
    { first: connection?.first, after: connection?.after },
    (record) => toUser(record),
  )
  return result
}

export const userQueryResolvers: QueryResolvers = {
  me: async (_parent, _args, context): Promise<User> => {
    const claim = await context.getClaim()
    assert(claim)
    const prisma = await context.getPrismaClient()
    const user = await prisma.user.findUnique({
      where: { id: claim.sub },
    })
    assert(user, new ForbiddenError(`Access denied`))
    return toUser(user)
  },
  users: async (_parent, args, context): Promise<User[]> => {
    const ability = await context.getAbility()
    assert(ability.can('read', 'User'), new ForbiddenError('Access denied'))
    const prisma = await context.getPrismaClient()
    const result = await prisma.user.findMany({
      where: { id: { in: args.ids } },
    })
    return result.map((r) => toUser(r))
  },
  allUsers: async (_parent, args, context): Promise<UserConnection> => {
    const ability = await context.getAbility()
    assert(ability.can('read', 'User'), new ForbiddenError('Access denied'))
    const where = await toUserWhereInput(context, args.filter)
    return toUserConnection(context, args.connection, where)
  },
  findUser: async (_parent, args, context): Promise<User | null> => {
    const ability = await context.getAbility()
    assert(ability.can('read', 'User'), new ForbiddenError('Access denied'))
    const prisma = await context.getPrismaClient()
    const result = await prisma.user.findUnique({
      where: { email: args.email },
    })
    return result ? toUser(result) : null
  },
}

export const userMutationResolvers: MutationResolvers = {
  connect: async (_parent, args, context): Promise<ConnectPayload> => {
    const claim = await context.getClaim()
    assert(claim?.isAdmin(), new ForbiddenError(`Access denied`))
    const prisma = await context.getPrismaClient()
    let isNew = false
    let user = await prisma.user.findUnique({
      where: { email: args.input.email },
    })
    if (!user) {
      user = await prisma.user.create({
        data: {
          email: args.input.email,
          type: ClaimType.NORMAL,
        },
      })
      isNew = true
    }
    assert(user?.enabled, new ForbiddenError(`Access denied`))
    const newClaim = new Claim(user.id, user.type)
    const token = await generateToken(newClaim)
    if (args.input.useCookie ?? true) {
      context.response.setHeader(
        'Set-Cookie',
        cookie.serialize('token', token, {
          httpOnly: true,
          expires: newClaim.exp ? new Date(newClaim.exp) : undefined,
        }),
      )
    }
    return {
      token,
      isNew,
      user: toUser(user),
    }
  },
  disconnect: async (_parent, _args, context): Promise<boolean> => {
    const claim = await context.getClaim()
    assert(claim, new ForbiddenError(`Access denied`))
    context.response.setHeader(
      'Set-Cookie',
      cookie.serialize('token', 'deleted', {
        httpOnly: true,
        expires: new Date(0),
      }),
    )
    await redisClient.del(claim.getKey())
    return true
  },
  updateMe: async (_parent, args, context): Promise<User> => {
    const claim = await context.getClaim()
    assert(claim, new ForbiddenError(`Access denied`))
    const prisma = await context.getPrismaClient()
    const result = await prisma.user.update({
      where: { id: claim.sub },
      data: {
        name: args.input.name,
        followers: {
          connectOrCreate: args.input.addFollow
            ? {
                where: {
                  followerId_followId: {
                    followId: args.input.addFollow,
                    followerId: claim.sub,
                  },
                },
                create: {
                  followId: args.input.addFollow,
                },
              }
            : undefined,
          delete: args.input.removeFollow
            ? {
                followerId_followId: {
                  followId: args.input.removeFollow,
                  followerId: claim.sub,
                },
              }
            : undefined,
        },
      },
    })
    assert(result, new ForbiddenError(`Access denied`))
    return toUser(result)
  },
  updateUser: async (_parent, args, context): Promise<User> => {
    const ability = await context.getAbility()
    assert(ability.can('update', 'User'), new ForbiddenError('Access denied'))
    const prisma = await context.getPrismaClient()
    const id = args.input.id
    const result = await prisma.user.update({
      where: { id },
      data: {
        name: args.input.name,
        enabled: args.input.enabled ?? undefined,
      },
    })
    if (!result) {
      throw new Error(`User id:${args.input.id} not found`)
    }
    return toUser(result)
  },
}

export const userResolvers: UserResolvers = {
  follows: async (parent, args, context): Promise<UserConnection> => {
    const where = await toUserWhereInput(context)
    where.follows = {
      some: { followerId: parent.id },
    }
    return toUserConnection(context, args.connection, where)
  },
  followers: async (parent, args, context): Promise<UserConnection> => {
    const where = await toUserWhereInput(context)
    where.followers = {
      some: { followId: parent.id },
    }
    return toUserConnection(context, args.connection, where)
  },
}

/** @type {import('tailwindcss').Config} */
module.exports = {
  // https://tailwindcss.com/docs/guides/nextjs
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    // https://daisyui.com/docs/install/
    require('daisyui'),
  ],
}

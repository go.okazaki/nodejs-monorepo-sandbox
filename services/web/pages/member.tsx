import { signIn, useSession } from 'next-auth/react'
import Head from 'next/head'
import useSWR from 'swr'
import { getApiService } from '../lib/api'

export default function Member() {
  const { data: session, status } = useSession()
  const { data } = useSWR('api/me', async () => {
    const apiService = await getApiService()
    return apiService.me()
  })
  if (status === 'unauthenticated') {
    signIn().catch((e) => console.warn(e))
  } else if (data) {
    return (
      <>
        <Head>
          <title>Member Page</title>
        </Head>
        <main>
          <h1>Member Page</h1>
          {data.me.email}
        </main>
      </>
    )
  }
}

import { ApiService, createApiService } from '@next-graphql-sandbox/core'
import { getSession } from 'next-auth/react'
import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig()

let apiService: ApiService

export async function getApiService(): Promise<ApiService> {
  if (!apiService) {
    apiService = createApiService(publicRuntimeConfig.apiEndpoint)
  }
  const session = await getSession()
  apiService.setToken(session?.accessToken)
  return apiService
}

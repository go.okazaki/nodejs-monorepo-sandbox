# Node.js Monorepo Sandbox

## Requirements

- [GNU make](https://www.gnu.org/software/make/manual/make.html)

- [Docker](https://www.docker.com/get-started/)

- [Docker Compose](https://docs.docker.com/compose/install/)

## Settings

- parallel make config

  - Linux

  ```sh
  export MAKEFLAGS=-j$(nproc)
  ```

  - macOS X

  ```sh
  export MAKEFLAGS=-j$(sysctl -n hw.physicalcpu)
  ```

## Development

- Build

```shell
make
```

- Execute

```shell
docker-compose up
```
